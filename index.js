const config = require('config');
const brokerConfig = config.get('BrokerConnection');
var stompit = require('stompit');
var log = require('simple-node-logger').createSimpleLogger('log.log');
log.setLevel(config.get('LogLevel'));


var connectOptions = config.get('BrokerConnection');

var connectionManager = new stompit.ConnectFailover(connectOptions);


////////////////////Failover
var channel = new stompit.Channel(connectionManager);

var subscribeHeaders = config.get('subscribeHeader');
console.log('success');

channel.subscribe('queue.test', function(error, message){
    if (error) {
        console.log('connect error ' + error.message);
        return;
      }
    message.readString('utf-8', function(error, body) {
        if (error) {
            console.log('read message error ' + error.message);
            return;
        }
        //masukkan logic proses message
        console.log('received message: ' + body);

        
        message.ack();
        log.info('message reveieved from ', ' success at ', new Date().toJSON());
    });
    
});